%{
 #include <stdio.h>

 int contador_td = 0;
 int contador_tr = 0;
 int contador_br = 0;
 int flag_segunda =0;

%}

VALIDOS [A-Z ,;0-9()ard+-]

%%
	/* para limpiar las etiquetas 
   \<td[^>]*\>  { printf("<td>");  }
   \<tr[^>]*\>  { printf("<tr>");  } 
  */

\<table\> { contador_td = contador_tr = 0; flag_segunda=1; }
\<b\> 
\<\/b\> 
\<td\>  { contador_td ++ ; }
\<tr\>  { contador_tr ++ ; } 
\<br\>  { contador_br ++ ; } 
\<\/td\> { contador_br = 0; } 
\<\/tr\> { contador_td = 0; }

{VALIDOS}+ { 
     if( contador_tr > 1 && contador_td > 1)
     {
      if(contador_br == 0 )
      {
       if(flag_segunda)
         printf("cb%x%x; %s; 2; 8 \n", contador_tr-2, contador_td-2, yytext);
       else
         printf("%x%x; %s; ", contador_tr-2, contador_td-2, yytext);
    
      }
      if(contador_br == 1 )
       if( !flag_segunda )
         printf("%s\n", yytext ); break;
     }
   } 
final	{ printf("(%d)(%d)%s", contador_tr, contador_td, yytext);} 
%%

int main(int n, char **args)
{
  FILE *fp = NULL;

  fp = fopen( *(args+1), "r");

  yyin = fp;

  yylex();

  return 0;
}

